(defsystem "aoc"
  :version "0.1.0"
  :author "hackeryarn"
  :license "GPL3"
  :depends-on ("function-cache"
               "for"
               "fset"
               "trivia"
               "trivia.ppcre")
  :components ((:module "src"
                :components
                ((:file "main")
                 (:module "2022"
                  :depends-on ("main")
                  :components ((:file "day16")
                               (:file "day17")
                               (:file "day18")
                               (:file "day19"))))))
  :description ""
  :in-order-to ((test-op (test-op "aoc/tests"))))

(defsystem "aoc/tests"
  :author "hackeryarn"
  :license "GPL3"
  :depends-on ("aoc"
               "rove")
  :components ((:module "tests"
                :components
                ((:file "main"))))
  :description "Test system for aoc"
  :perform (test-op (op c) (symbol-call :rove :run c)))
