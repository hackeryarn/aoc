(defpackage day20
  (:use :cl :trivia :trivia.ppcre))
(in-package :day20)

(defun process-stream (in)
  (loop for line = (read-line in nil)
        while line
        collect (parse-integer line)))

(defun read-test-input (str)
  (with-input-from-string (in str)
    (process-stream in)))

(defparameter *test-input*
  (read-test-input
   "1
2
-3
3
-2
0
4"))

(defun read-input (file)
  (with-open-file (in file)
    (process-stream in)))

(defparameter *input* (read-input "resources/day20.txt"))

(defun make-table (numbers)
  (make-array (length numbers) :initial-contents numbers))

(defun make-seq (numbers)
  (let ((sequence (loop for i from 0 below (length numbers) collect i)))
    (setf (cdr (last sequence)) sequence)))

(defun mix (table sequence)
  (loop for i from 0 below (length table)
        for previous = (loop for pos on sequence
                             until (= (cadr pos) i)
                             finally (return pos))
        for element = (cdr previous)
        for value = (aref table (car element))
        for shift-by = (mod value (1- (length table)))
        for new-previous = (nthcdr shift-by element)
        unless (zerop (mod value (1- (length table))))
          do (shiftf (cdr previous) (cdr element)
                  (cdr new-previous) element)))

(defun get-answer (table sequence)
  (let* ((zero (position 0 table))
         (start (loop for pos on sequence
                      until (= (car pos) zero)
                      finally (return pos))))
    (+ (aref table (elt start 1000))
       (aref table (elt start 2000))
       (aref table (elt start 3000)))))

(defun part1 (numbers)
  (let ((table (make-table numbers))
        (sequence (make-seq numbers)))
    (mix table sequence)
    (get-answer table sequence)))

(defun part2 (numbers &key (decryption-key 811589153) (cycles 10))
  (let ((table (make-table numbers))
        (sequence (make-seq numbers)))
    (loop for i from 0 below (length table)
          for n across table
          do (setf (aref table i) (* decryption-key n)))
    (loop repeat cycles
          do (mix table sequence))
    (get-answer table sequence)))
