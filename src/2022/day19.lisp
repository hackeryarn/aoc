(defpackage day19
  (:use :cl :trivia :trivia.ppcre :function-cache))
(in-package day19)

(defun process-stream (in)
  (loop for  line = (read-line in nil)
        while line
        collect (mapcar #'parse-integer (cl-ppcre:all-matches-as-strings "\\d+" line))))

(defun read-test-input (str)
  (with-input-from-string (in str)
    (process-stream in)))

(defparameter *test-input*
  (read-test-input
   "Blueprint 1: Each ore robot costs 4 ore. Each clay robot costs 2 ore. Each obsidian robot costs 3 ore and 14 clay. Each geode robot costs 2 ore and 7 obsidian.
Blueprint 2: Each ore robot costs 2 ore. Each clay robot costs 3 ore. Each obsidian robot costs 3 ore and 8 clay. Each geode robot costs 3 ore and 12 obsidian."))

(defun read-input (file)
  (with-open-file (in file)
    (process-stream in)))

(defparameter *input* (read-input "resources/day19.txt"))

(defun quality-level (blueprint &optional (base-time 24))
  (let ((best 0))
    (match blueprint
      ((list n ore-ore clay-ore obsidian-ore obsidian-clay geode-ore geode-obsidian)
       (labels ((recur (ore clay obsidian geode ore-bot clay-bot obsidian-bot geode-bot &optional (time base-time))
                  (let ((estimate (+ geode (* geode-bot time)
                                     (if (and (<= geode-ore ore)
                                              (<= geode-obsidian obsidian))
                                         (/ (* (1- time) time) 2)
                                         (/ (* (- time 2) (1- time)) 2)))))
                    (setf best (max best geode))
                    (cond ((<= estimate best) best)
                          ((zerop time) best)
                          (t
                           (max
                            (if (and (<= geode-ore ore)
                                     (<= geode-obsidian obsidian))
                                (recur (+ ore ore-bot (- geode-ore))
                                       (+ clay clay-bot)
                                       (+ obsidian obsidian-bot (- geode-obsidian))
                                       (+ geode geode-bot)
                                       ore-bot clay-bot obsidian-bot (1+ geode-bot)
                                       (1- time))
                                0)
                            (if (and (<= obsidian-ore ore)
                                     (<= obsidian-clay clay))
                                (recur (+ ore ore-bot (- obsidian-ore))
                                       (+ clay clay-bot (- obsidian-clay))
                                       (+ obsidian obsidian-bot)
                                       (+ geode geode-bot)
                                       ore-bot clay-bot (1+ obsidian-bot) geode-bot
                                       (1- time))
                                0)
                            (if (and (<= clay-ore ore))
                                (recur (+ ore ore-bot (- clay-ore))
                                       (+ clay clay-bot)
                                       (+ obsidian obsidian-bot)
                                       (+ geode geode-bot)
                                       ore-bot (1+ clay-bot) obsidian-bot geode-bot
                                       (1- time))
                                0)
                            (if (and (<= ore-ore ore))
                                (recur (+ ore ore-bot (- ore-ore))
                                       (+ clay clay-bot)
                                       (+ obsidian obsidian-bot)
                                       (+ geode geode-bot)
                                       (1+ ore-bot) clay-bot obsidian-bot geode-bot
                                       (1- time))
                                0)
                            (recur (+ ore ore-bot)
                                   (+ clay clay-bot)
                                   (+ obsidian obsidian-bot)
                                   (+ geode geode-bot)
                                   ore-bot clay-bot obsidian-bot geode-bot
                                   (1- time))))))))
         (recur 0 0 0 0 1 0 0 0)
         (values (* n best) n best))))))

(defun part1 (blueprints &optional (time 24))
  (loop for blueprint in blueprints
        sum (time (quality-level blueprint time))))

(defun part2 (blueprints &optional (time 32))
  (let ((remaining (subseq blueprints 0 1)))
    (loop for blueprint in remaining
          collect (multiple-value-match (time (quality-level blueprint time))
                    ((_ _ best) best)) into bests
          finally (print bests)
                  (return (apply #'* bests)))))
