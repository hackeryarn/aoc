(defpackage day17
  (:use :cl :trivia :trivia.ppcre))
(in-package day17)

(defparameter *test-input*
  ">>><<><>><<<>><>>><<<>>><<<><<<>><>><<>>")

(defun read-input (file)
  (with-open-file (in file)
    (read-line in)))

(defparameter *input*
  (read-input "resources/day17.txt"))

(defun piece1 (level)
  (loop for i from 3
        repeat 4
        collect (complex i level)))

(defun piece2 (level)
  (list (complex 4 level)
        (complex 3 (1+ level))
        (complex 4 (1+ level))
        (complex 5 (1+ level))
        (complex 4 (+ level 2))))

(defun piece3 (level)
  (list (complex 3 level)
        (complex 4 level)
        (complex 5 level)
        (complex 5 (+ level 1))
        (complex 5 (+ level 2))))

(defun piece4 (level)
  (loop for i from 0
        repeat 4
        collect (complex 3 (+ level i))))

(defun piece5 (level)
  (loop for i from 3 to 4
        with result = nil
        finally (return result)
        do (loop for j from 0 to 1
                 do (push (complex i (+ level j)) result))))

(defun infinite-pieces ()
  (let ((functions (list #'piece1 #'piece2 #'piece3 #'piece4 #'piece5)))
    (setf (cdr (last functions)) functions)
    (lambda (level)
      (funcall (pop functions) level))))

(defun infinite-move (moves)
  (let ((moves (loop for c across moves
                     collect (case c
                               (#\< -1)
                               (#\> 1)))))
    (setf (cdr (last moves)) moves)
    moves))

(defun drop (piece)
  (loop for part in piece
        collect (- part (complex 0 1))))

(defun blow (piece dir)
  (loop for part in piece
        collect (+ part dir)))

(defun can-fall-p (piece world)
  (loop for part in piece
        never (gethash (- part (complex 0 1)) world)
        never (= 1 (imagpart part))))

(defun can-shit-p (piece world dir)
  (loop for part in piece
        never (gethash (+ part dir) world)
        always (<= 1 (+ dir (realpart part)) 7)))

(defun add-piece-to-world (piece world)
  (loop for part in piece
        do (setf (gethash part world) t)
        maximizing (imagpart part)))

(defun calc-height (moves &optional (number-pieces 2022))
  (loop with moves = (infinite-move moves)
        with pieces = (infinite-pieces)
        with level = 4
        with world = (make-hash-table)
        repeat number-pieces
        finally (return (values (- level 4) world))
        do (loop for wind = (pop moves)
                 with piece = (funcall pieces level)
                 if (can-shit-p piece world wind)
                   do (setf piece (blow piece wind))
                 while (can-fall-p piece world)
                 do (setf piece (drop piece))
                 finally (setf level (max level (+ 4 (add-piece-to-world piece world)))))))

;; part 2

(defun full-rows (level world)
  (loop for y from level downto 1
        if (loop for x from 1 to 7
                 always (gethash (complex x y) world))
          collect (print y)))

(defun full-row (level world)
  (loop for x from 1 to 7
        always (gethash (complex x level) world)))

(defun compare-sections (world lower middle upper)
  (loop for a from lower below middle
        for b from middle below upper
        always (loop for x from 1 to 7
                     for apart = (complex x a)
                     for bpart = (complex x b)
                     always (and (if (gethash apart world)
                                     (gethash bpart world)
                                     t)
                                 (if (gethash bpart world)
                                     (gethash apart world)
                                     t)))))

(defun find-cycle-time ()
  (multiple-value-bind (level world) (calc-height *input* 10000)
    (let ((full-rows (loop with previous = 0
                          with result = '()
                          for current in (full-rows level world)
                          finally (return result)
                          do (push (list (- previous current) current) result)
                             (setf previous current))))
      (format t "Full rows: ~A~%" full-rows))
    (format t "Level: ~A~%" level)
    (format t "Sections match: ~A~%" (compare-sections world 390 3057 5724))))

(defun calc-height-detailed (moves &optional (number-pieces 2022))
  (loop with moves = (infinite-move moves)
        with pieces = (infinite-pieces)
        with level = 4
        with world = (make-hash-table)
        with count = 0
        with found-390 = nil
        with found-3057 = nil
        repeat number-pieces
        finally (return (values (- level 4) world))
        when (and (not found-390) (>= (- level 4) 390))
          do (format t "~A (~A)~&" count (- level 4))
             (setf found-390 t)
        when (and (not found-3057) (>= (- level 4) 3057))
          do (format t "~A (~A)~&" count (- level 4))
             (setf found-3057 t)
        when (= count (+ 256 1619))
          do (format t "non cycle height: ~A~&" (- level 4 390))
        do (loop for wind = (pop moves)
                 with piece = (funcall pieces level)
                   initially (incf count)
                 if (can-shit-p piece world wind)
                   do (setf piece (blow piece wind))
                 while (can-fall-p piece world)
                 do (setf piece (drop piece))
                 finally (setf level (max level (+ 4 (add-piece-to-world piece world)))))))

;;; 256 pieces is the start of the first cycle with the height of 390.
;;; 1991 is the start of the next cycle with the height of 3057.
;;; after running the cycles, there are 1619 pieces still to drop.
;;; 2870 is the hiehgt of all the pieces not in the cycle 256 + 1619.
(+ 2870
   (* (- 3057 390)
      (floor (/ (- 1000000000000 256)
                (- 1991 256)))))
