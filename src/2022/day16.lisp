(defpackage day16
  (:use :cl :trivia :trivia.ppcre))
(in-package :day16)

(defstruct valve
  id
  rate
  tunnels)

(defun parse-line (line)
  (match line
    ((ppcre "Valve ([A-Z]{2}) has flow rate=(\\d+); tunnels? leads? to valves? (.*)"
            id (read rate) tunnels)
     (make-valve :id id
                 :rate rate
                 :tunnels (split ", " tunnels)))))

(defun process-stream (in)
  (loop for line = (read-line in nil)
        while line
        collect (parse-line line)))

(defun read-input (file)
  (with-open-file (in file)
    (process-stream in)))

(defun read-test-input (str)
  (with-input-from-string (in str)
    (process-stream in)))

(defparameter *input*
  (read-input "resources/day16.txt"))

(defparameter *test-input*
  (read-test-input
   "Valve AA has flow rate=0; tunnels lead to valves DD, II, BB
Valve BB has flow rate=13; tunnels lead to valves CC, AA
Valve CC has flow rate=2; tunnels lead to valves DD, BB
Valve DD has flow rate=20; tunnels lead to valves CC, AA, EE
Valve EE has flow rate=3; tunnels lead to valves FF, DD
Valve FF has flow rate=0; tunnels lead to valves EE, GG
Valve GG has flow rate=0; tunnels lead to valves FF, HH
Valve HH has flow rate=22; tunnel leads to valve GG
Valve II has flow rate=0; tunnels lead to valves AA, JJ
Valve JJ has flow rate=21; tunnel leads to valve II"))

(defun maximize-pressure (paths rates bit-map initial-time initial-open)
  (loop with fringe = (list (list "AA" initial-time 0 initial-open))
        with table = (make-hash-table :test #'equalp)
        for (position time pressure open) = (pop fringe)
        for state = (list position time open)
        for rate = (gethash position rates)
        for increase = (* (1- time) rate)
        for next-pressure = (+ pressure increase)
        when (zerop time)
          maximizing pressure
        if (and (< (gethash state table -1) pressure) (plusp time))
          do (setf (gethash state table) pressure)
             (loop for destination in (gethash position paths)
                   for next = (list destination (1- time) pressure open)
                   for next-rate = (gethash destination rates)
                   for estimate = (* (- time 2) next-rate)
                   do (push next fringe))
             (if (and (zerop (logand (gethash position bit-map) open))
                      (plusp rate))
                 (push (list position (1- time) next-pressure (logior open (gethash position bit-map)))
                       fringe))
        until (not fringe)))

(defun valves-to-paths (data)
  (loop with paths = (make-hash-table :test 'equalp)
        with rates = (make-hash-table :test 'equalp)
        with bit-map = (make-hash-table :test 'equalp)
        for i from 0
        for valve in data
        do (let ((id (valve-id valve)))
             (setf (gethash id rates) (valve-rate valve))
             (setf (gethash id paths) (valve-tunnels valve))
             (setf (gethash id bit-map) (expt 2 i)))
        finally (return (list paths rates bit-map))))

(defun part1 (data &optional (initial-time 30) (initial-open 0))
  (match (valves-to-paths data)
    ((list paths rates bit-map)
     (maximize-pressure paths rates bit-map initial-time initial-open))))


(defun all-pressure (paths rates bit-map initial-time initial-open)
  (loop with fringe = (list (list "AA" initial-time 0 initial-open))
        with table = (make-hash-table :test #'equalp)
        for (position time pressure open) = (pop fringe)
        for state = (list position time open)
        for rate = (gethash position rates)
        for increase = (* (1- time) rate)
        for next-pressure = (+ pressure increase)
        with result = (make-hash-table)
        finally (return result)
        when (zerop time)
          do (setf (gethash open result) (max (gethash open result 0) pressure))
        if (and (< (gethash state table -1) pressure) (plusp time))
          do (setf (gethash state table) pressure)
             (loop for destination in (gethash position paths)
                   for next = (list destination (1- time) pressure open)
                   for next-rate = (gethash destination rates)
                   for estimate = (* (- time 2) next-rate)
                   do (push next fringe))
             (if (and (zerop (logand (gethash position bit-map) open))
                      (plusp rate))
                 (push (list position (1- time) next-pressure (logior open (gethash position bit-map)))
                       fringe))
        until (not fringe)))

(defun part2 (data)
  (match (valves-to-paths data)
    ((list paths rates bit-map)
     (let ((pairs (all-pressure paths rates bit-map 26 0)))
       (loop for o1 being the hash-key of pairs using (hash-value p1)
             maximizing (loop for o2 being the hash-key of pairs using (hash-value p2)
                              if (zerop (logand o1 o2))
                                maximizing (+ p1 p2)))))))

(part1 *test-input*)
