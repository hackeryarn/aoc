(defpackage day21
  (:use :cl :trivia :trivia.ppcre))
(in-package day21)

(defun process-stream (in)
  (loop for line = (read-line in nil)
        while line
        collect line))

(defun read-test-input (str)
  (with-input-from-string (in str)
    (process-stream in)))

(defparameter *test-input*
  (read-test-input
   "root: pppw + sjmn
dbpl: 5
cczh: sllz + lgvd
zczc: 2
ptdq: humn - dvpt
dvpt: 3
lfqf: 4
humn: 5
ljgn: 2
sjmn: drzm * dbpl
sllz: 4
pppw: cczh / lfqf
lgvd: ljgn * ptdq
drzm: hmdt - zczc
hmdt: 32"))

(defun read-input (file)
  (with-open-file (in file)
    (process-stream in)))

(defparameter *input*
  (read-input "resources/day21.txt"))

(defun get-monkey (monkeys name)
  (let ((monkey (gethash name monkeys)))
    (cond ((numberp monkey)
           monkey)
          ((functionp monkey)
           (funcall monkey)))))

(defun process-job (monkeys job)
  (match job
    ((ppcre "(\\w+): (\\d+)"
            name (read number))
     (setf (gethash name monkeys) number))
    ((ppcre "(\\w+): (\\w+) (.{1}) (\\w+)"
            name lhs (read op) rhs)
     (setf (gethash name monkeys)
           (lambda ()
             (let ((l (get-monkey monkeys lhs))
                   (r (get-monkey monkeys rhs)))
               (funcall op l r)))))))

(defun part1 (jobs)
  (let ((monkeys (make-hash-table :test #'equal)))
    (loop for job in jobs
          do (process-job monkeys job))
    (get-monkey monkeys "root")))

;;; Part 2

(defun process-job2 (monkeys job left right)
  (match job
    ((ppcre "(\\w+): (\\d+)"
            name (read number))
     (setf (gethash name monkeys) number))
    ((ppcre "root: (\\w+) . (\\w+)"
            lhs rhs)
     (setf left lhs)
     (setf right rhs)
     (setf (gethash "root" monkeys)
           (lambda ()
             (let ((l (get-monkey monkeys lhs))
                   (r (get-monkey monkeys rhs)))
               (= l r)))))
    ((ppcre "(\\w+): (\\w+) (.) (\\w+)"
            name lhs (read op) rhs)
     (setf (gethash name monkeys)
           (lambda ()
             (let ((l (get-monkey monkeys lhs))
                   (r (get-monkey monkeys rhs)))
               (funcall op l r))))))
  (values left right))

(defun part2 (jobs)
  (let ((monkeys (make-hash-table :test #'equal))
        factor
        start
        left
        right)
    (labels ((get-monkey (name)
               (let ((monkey (gethash name monkeys)))
                 (cond ((numberp monkey)
                        monkey)
                       ((functionp monkey)
                        (funcall monkey)))))
             (process-job (job)
              (match job
                ((ppcre "(\\w+): (\\d+)"
                        name (read number))
                 (setf (gethash name monkeys) number))
                ((ppcre "root: (\\w+) . (\\w+)"
                        lhs rhs)
                 (setf left lhs)
                 (setf right rhs)
                 (setf (gethash "root" monkeys)
                       (lambda ()
                         (let ((l (get-monkey lhs))
                               (r (get-monkey rhs)))
                           (= l r)))))
                ((ppcre "(\\w+): (\\w+) (.) (\\w+)"
                        name lhs (read op) rhs)
                 (setf (gethash name monkeys)
                       (lambda ()
                         (let ((l (get-monkey lhs))
                               (r (get-monkey rhs)))
                           (funcall op l r))))))
              (values left right)))

      (for:for ((job over jobs))
        (process-job job))
      (setf (gethash right monkeys) (get-monkey right))
      ;; Search for the factor to increase by
      (for:for ((humn from 0))
        (setf (gethash "humn" monkeys) humn)
        (until (integerp (get-monkey left)))
        (returning (setf start humn)))
      (for:for ((humn from (1+ start)))
        (setf (gethash "humn" monkeys) humn)
        (until (integerp (get-monkey left)))
        (returning (setf factor (- humn start))))
      (let (first second)
        (setf (gethash "humn" monkeys) start)
        (setf first (get-monkey left))
        (setf (gethash "humn" monkeys) (+ start factor))
        (setf second (get-monkey right))
        (if (< first second)
            (for:for ((humn as start)
                      (step as 1)
                      (i repeat 100))
              (setf (gethash "humn" monkeys) humn)
              (until (get-monkey "root"))
              (returning humn)
              (cond
                ((< (get-monkey left) (get-monkey right))
                 (incf step step)
                 (incf humn (* step factor)))
                (t
                 (setf step (max 1 (floor step 2)))
                 (decf humn (* step factor)))))
            (for:for ((humn as start)
                      (step as 1))
              (setf (gethash "humn" monkeys) humn)
              (until (get-monkey "root"))
              (returning humn)
              (cond
                ((< (get-monkey right) (get-monkey left))
                 (incf step step)
                 (incf humn (* step factor)))
                (t
                 (setf step (max 1 (floor step 2)))
                 (decf humn (* step factor))))))))))
