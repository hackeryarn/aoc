(defpackage day18
  (:use :cl :trivia :trivia.ppcre))
(in-package day18)

(defun-match parse-line (line)
  ((split "," (read x) (read y) (read z))
   (list x y z)))

(defun process-stream (in)
  (loop for line = (read-line in nil)
        while line
        collect (parse-line line)))

(defun read-input (file)
  (with-open-file (in file)
    (process-stream in)))

(defparameter *input*
  (read-input "resources/day18.txt"))

(defun read-test-input (str)
  (with-input-from-string (in str)
    (process-stream in)))

(defparameter *test-input*
  (read-test-input
   "2,2,2
1,2,2
3,2,2
2,1,2
2,3,2
2,2,1
2,2,3
2,2,4
2,2,6
1,2,5
3,2,5
2,1,5
2,3,5"))

(defun part1 (cubes)
  (loop for cube in cubes
        for (x y z) = cube
        for rest = (cdr cubes) then (cdr rest)
        with total = (* 6 (length cubes))
        do (loop for dx in '(-1 1)
                 for neighbor = (list (+ x dx) y z)
                 if (member neighbor rest :test #'equal)
                   do (decf total 2))
        do (loop for dy in '(-1 1)
                 for neighbor = (list x (+ y dy) z)
                 if (member neighbor rest :test #'equal)
                   do (decf total 2))
        do (loop for dz in '(-1 1)
                 for neighbor = (list x  y (+ z dz))
                 if (member neighbor rest :test #'equal)
                   do (decf total 2))
        finally (return total)))

(defun list-to-grid (cubes)
  (loop with grid = (make-hash-table :test #'equalp)
        for cube in cubes
        do (setf (gethash cube grid) #\#)
        finally (return grid)))

(defun bounds (cubes)
  (loop for (x y z) in cubes
        minimizing x into min-x
        minimizing y into min-y
        minimizing z into min-z
        maximizing x into max-x
        maximizing y into max-y
        maximizing z into max-z
        finally (return (list (list min-x min-y min-z)
                              (list max-x max-y max-z)))))

(defun in-bounds-p (cube min max)
  (every #'<= min cube max))

(defun-match* exterior-p (cube grid min max)
  (((list x y z))
   (or
    (loop for dx from 1
          for option = (list (+ x dx) y z)
          while (in-bounds-p option min max)
          never (gethash option grid))
    (loop for dx from -1
          for option = (list (+ x dx) y z)
          while (in-bounds-p option min max)
          never (gethash option grid))
    (loop for dy from 1
          for option = (list x (+ y dy) z)
          while (in-bounds-p option min max)
          never (gethash option grid))
    (loop for dy from -1
          for option = (list x (+ y dy) z)
          while (in-bounds-p option min max)
          never (gethash option grid))
    (loop for dz from 1
          for option = (list x y (+ z dz))
          while (in-bounds-p option min max)
          never (gethash option grid))
    (loop for dz from -1
          for option = (list x y (+ z dz))
          while (in-bounds-p option min max)
          never (gethash option grid)))))

(defun-match neighbors (cube)
  ((list x y z) cube
   (append
    (loop for dx in '(-1 1)
          collect (list (+ x dx) y z))
    (loop for dy in '(-1 1)
          collect (list x (+ y dy) z))
    (loop for dz in '(-1 1)
          collect (list x y (+ z dz))))))


(defun fill-space (cube grid min max)
  (loop with queue = (list cube)
        with visited = (make-hash-table :test #'equal)
        for c = (pop queue)
        do (setf (gethash c visited) t)
        if (exterior-p c grid min max)
          return nil
        do (loop for n in (neighbors c)
                 unless (or (gethash n visited)
                            (gethash n grid))
                   do (push n queue))
        if (zerop (length queue))
          do (loop for k being the hash-key of visited
                   do (setf (gethash k grid) t))
             (return nil)))

(defun total-surface-area (grid)
  (loop for cube being the hash-key of grid
        for (x y z) = cube
        with total = (* 6 (hash-table-count grid))
        do (loop for dx in '(-1 1)
                 for neighbor = (list (+ x dx) y z)
                 if (gethash neighbor grid)
                   do (decf total))
        do (loop for dy in '(-1 1)
                 for neighbor = (list x (+ y dy) z)
                 if (gethash neighbor grid)
                   do (decf total))
        do (loop for dz in '(-1 1)
                 for neighbor = (list x  y (+ z dz))
                 if (gethash neighbor grid)
                   do (decf total))
        finally (return total)))

(defun part2 (cubes)
  (let ((grid (list-to-grid cubes)))
    (match (bounds cubes)
      ((list (list min-x min-y min-z)
             (list max-x max-y max-z))
       (loop with min = (list min-x min-y min-z)
             with max = (list max-x max-y max-z)
             for x from min-x to max-x
             do (loop for y from min-y to max-y
                      do (loop for z from min-z to max-z
                               for cube = (list x y z)
                               unless (gethash cube grid)
                                 do (fill-space cube grid min max))))
       (total-surface-area grid)))))
